/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package saxparsing;

import java.util.Vector;

/**
 *
 * @author Matías
 */
class SesionesPelicula {
    
    private Pelicula pelicula;
    private Vector sesionesPelicula;
    
    public SesionesPelicula(Pelicula pelicula, Vector sesionesPelicula) {
       this.pelicula = pelicula;
       this.sesionesPelicula = sesionesPelicula;
    }

    public Pelicula getPelicula() {
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public Vector getSesionesPelicula() {
        return sesionesPelicula;
    }

    public void setSesionesPelicula(Vector sesionesPelicula) {
        this.sesionesPelicula = sesionesPelicula;
    }
    
    
    
}
